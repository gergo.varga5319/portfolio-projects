package com.repository;

import com.entity.Person;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public class PersonRepositoryImplementation implements PersonRepository {

  // do not
  private Session session; // hibernate session type!!

  // better way not to put the opensession() in the constructor but create a method for it
  public PersonRepositoryImplementation() {
    session =
        HibernateUtil.getSessionFactory()
            .openSession(); // get the sessionFactory from HibernateUtil class to create the session
                            // '= the connection'
  }

  // close the session in that class where you open the session
  public void closeSession() {
    session.close();
  }

  @Override
  public Person getByID(int id) {
      return session.get(Person.class, id);
  }

  @Override
  public void save(Person person) {
    session.beginTransaction();
    session.save(person);
    session.getTransaction().commit();
  }

  @Override
  public void update(Person person) {
    session.beginTransaction();
    session.update(person);
    session.getTransaction().commit();
  }

  @Override
  public void delete(Person person) {
    session.beginTransaction();
    session.delete(person);
    session.getTransaction().commit();
  }

  @Override
  @SuppressWarnings("unchecked")
  public List<Person> getAllPersonsWithHQL() {
      return (List<Person>) session.createQuery("FROM Person").list();
  }

  @Override
  public List<Person> getAllPersons() {
    CriteriaQuery<Person> criteriaQuery = session.getCriteriaBuilder().createQuery(Person.class);
    criteriaQuery.from(Person.class);
      return session.createQuery(criteriaQuery).getResultList();
  }

  @Override
  public List<Person> getAllPersonsDeprecated() {
      return (List<Person>) session.createCriteria(Person.class).list();
  }
}
