package com.repository;

import com.entity.Person;
import java.util.List;

public interface PersonRepository {

  Person getByID(int id);

  void save(Person person);

  void update(Person person);

  void delete(Person person);

  List<Person> getAllPersonsWithHQL();

  List<Person> getAllPersons();

  List<Person> getAllPersonsDeprecated();
}
