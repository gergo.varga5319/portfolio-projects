package com.matritel.resttodo.repository.entity;

import com.matritel.resttodo.model.Priority;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;

@Entity(name = "todo")
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TodoEntity {

    /*@Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;*/

    @Id
    @GenericGenerator(name = "UseExistingIdOtherwiseGenerateUsingIdentity", strategy = "com.matritel.resttodo.service.UseExistingIdOtherwiseGenerateUsingIdentity")
    @GeneratedValue(generator = "UseExistingIdOtherwiseGenerateUsingIdentity")
    private String id;

    @Column(name = "description", columnDefinition = "VARCHAR(255)")
    private String description;

    @Column(name = "person_in_charge", columnDefinition = "VARCHAR(40)")
    private String personInCharge;

    @Enumerated(EnumType.STRING)
    @Column(length = 8)
    private Priority priority;

    @Column(name = "created_at")
    private LocalDate createdAt;
}
