package com.matritel.resttodo.repository;

import com.matritel.resttodo.model.Priority;
import com.matritel.resttodo.repository.entity.TodoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

//the second thing in the CrudRepo is the type of the ID
@Repository
public interface ToDoRepository extends CrudRepository<TodoEntity, String> {
    //This method will be implemented because 'findAll' is already in the in the CrudRepository.
    // With changing the return type of findAll, Spring will recognize and implement the method
    List<TodoEntity> findAll();

    List<TodoEntity> findAllByPriority(Priority priority);
}
