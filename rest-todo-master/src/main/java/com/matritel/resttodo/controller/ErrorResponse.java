package com.matritel.resttodo.controller;

import lombok.*;

@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@Setter
@Getter
public class ErrorResponse {
    //private HttpStatus errorCode;
    private String message;

    public ErrorResponse(String message) {
        this.message = message;
    }
}
