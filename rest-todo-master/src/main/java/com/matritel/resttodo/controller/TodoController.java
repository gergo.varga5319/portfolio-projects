package com.matritel.resttodo.controller;


import com.matritel.resttodo.model.Priority;
import com.matritel.resttodo.model.TodoDto;
import com.matritel.resttodo.service.TodoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
//If we use RestController we don't need to annotate ResponseBody for the methods
@RestController
@RequestMapping(value = "api/matritel/todos")
public class TodoController {

    @Autowired
    private TodoService todoService;

    // IT's a good convention to use http requests as method name
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoDto postTodo(@Valid @RequestBody TodoDto todoInput) {
        return todoService.create(todoInput);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.CREATED)
    public TodoDto putTodo(@Valid @RequestBody TodoDto todoInput, @PathVariable String id) {
        return todoService.put(todoInput, id);
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public List<TodoDto> getAllTodos() {
        return todoService.findAll();
    }

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public TodoDto getTodoById(@PathVariable String id) {
        Optional<TodoDto> optionalTodoDto = todoService.findById(id);
        if (optionalTodoDto.isPresent()) {
            return optionalTodoDto.get();
        }
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "NOT FOUND");
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deleteTodoById(@PathVariable String id) {

        todoService.deleteById(id);
    }

    @RequestMapping("/findByPriority")
    @ResponseStatus(HttpStatus.OK)
    public List<TodoDto> findAllByPriority(@RequestParam(name = "priority") Priority priority) {

        return todoService.findAllByPriority(priority);

    }

    @RequestMapping("/findByPic")
    @ResponseStatus(HttpStatus.OK)
    public List<TodoDto> findAllByPersonInCharge(@RequestParam(name = "person") String personInCharge) {

        // syntax of the endpoint, e.g.: localhost:8080/api/matritel/todos/findByPic/?person=Zsombor
        return todoService.findAllByPIC(personInCharge);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse mapMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {

        return new ErrorResponse("This is not a valid Priority");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse mapMethodArgumentNotValidException(MethodArgumentNotValidException ex) {

        return new ErrorResponse("This is a bad input for a todo");
    }

    @ExceptionHandler(ResponseStatusException.class)
    //@ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse mapResponseStatusException(ResponseStatusException ex) {

        return new ErrorResponse();
    }

    @ExceptionHandler(RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse mapInternalServerError() {
        return new ErrorResponse("Internal server error");
    }


}
