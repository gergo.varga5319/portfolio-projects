package com.matritel.resttodo.model;

public enum Priority {

    URGENT, HIGH, MEDIUM, LOW
}
