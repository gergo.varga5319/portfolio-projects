package com.matritel.rpg_game.board;

class Wall extends GameObject {
    Wall(int posX, int posY) {
        super("wall.png", posX, posY);
    }
}
