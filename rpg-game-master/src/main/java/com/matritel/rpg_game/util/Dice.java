package com.matritel.rpg_game.util;

import java.util.Random;

public class Dice {

    public int doRandom(int bound) {
        Random r = new Random();
        return r.nextInt(bound);
    }

}
