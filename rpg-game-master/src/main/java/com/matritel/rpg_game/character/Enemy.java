package com.matritel.rpg_game.character;

import com.matritel.rpg_game.util.Dice;

public abstract class Enemy extends Character {

    Enemy(String filename, int posX, int posY) {
        super(filename, posX, posY);
        this.dice = new Dice();
    }

    public void move(int[][] board) {
        //enemy type characters are moving when hero makes a step.
        //they always move one step to left right up or deown depending on the result of a random number between 1-4
        //the new coordinates are stored in a 2 dimensional array.
        //for example: if the random number is zero, then the X coordinates of the character will not change
        // but hte Y coordinate will be increased by 1.
        //current X and Y values will be udpated accordingly and function makes sure the X and Y coordinates are valid
        // they are on the the board, and not on the wall.
        int[][] steps = new int[4][2];
        //move down
        //X
        steps[0][0] = 0;
        //Y
        steps[0][1] = 1;
        //move up
        //X
        steps[1][0] = 0;
        //Y
        steps[1][1] = -1;
        // move left
        //X
        steps[2][0] = -1;
        //Y
        steps[2][1] = 0;
        // move right
        //X
        steps[3][0] = 1;
        //Y
        steps[3][1] = 0;


        int nextStep = dice.doRandom(4);
        int extraX = steps[nextStep][0];
        int extraY = steps[nextStep][1];
        int newX = this.getPosX() + extraX;
        int newY = this.getPosY() + extraY;

        //make sure not to leave the area/field/board
        if ((newX > -1 && newY > -1) && (newX < 10 && newY < 10)) {
            //make sure it cannot step on wall
            if (board[newY][newX] == 1) {
                this.setPosX(newX);
                this.setPosY(newY);
            }
        }
    }
}
