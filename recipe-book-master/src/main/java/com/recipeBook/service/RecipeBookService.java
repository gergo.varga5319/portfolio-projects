package com.recipeBook.service;

import com.recipeBook.domain.RecipeDTO;
import com.recipeBook.repository.RecipeBookRepository;
import com.recipeBook.repository.entity.RecipeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RecipeBookService {

    @Autowired
    private RecipeBookRepository recipeBookRepository;


    public RecipeDTO create(RecipeDTO recipeDTO) {
        RecipeEntity toSave = toEntity(recipeDTO); //from DTO we create   an entity to able to save it || here it gets its createdAt (localDate)
        RecipeEntity saved = recipeBookRepository.save(toSave);//we save  into repository || here ti gets its ID
        return recipeToDTO(saved);
    }


    private RecipeDTO recipeToDTO(RecipeEntity recipeEntity) {
        return RecipeDTO.builder()
                .id(recipeEntity.getId())
                .title(recipeEntity.getTitle())
                .instructions(recipeEntity.getInstructions())
                .ingredients(recipeEntity.getIngredients())
                .creatorName(recipeEntity.getCreatorName())
                .localDate(recipeEntity.getLocalDate())
                .build();


    }

    private RecipeEntity toEntity(RecipeDTO recipeInput) {
        return RecipeEntity.builder()
                .id(recipeInput.getId())
                .title(recipeInput.getTitle())
                .instructions(recipeInput.getInstructions())
                .ingredients(recipeInput.getIngredients())
                .creatorName(recipeInput.getCreatorName())
                .localDate(LocalDate.now())
                .build();

    }


    public List<RecipeDTO> findAll() {
        List<RecipeDTO> recipeDtoList = new ArrayList<>();
        for (RecipeEntity entity : recipeBookRepository.findAll()) {
            recipeDtoList.add(recipeToDTO(entity));
        }
        return recipeDtoList;
    }


    public Optional<RecipeDTO> findByID(String id) {

        Optional<RecipeEntity> recipe = recipeBookRepository.findById(id);
        return recipe.map(this::recipeToDTO);

    }


    public void deleteById(String id) {
        recipeBookRepository.deleteById(id);
    }


    public List<RecipeDTO> findByCreator(String creator) {
        List<RecipeDTO> listOfRecipes = new ArrayList<>();

        for (RecipeEntity reicpe : recipeBookRepository.findAll()) {
            if (reicpe.getCreatorName().equals(creator)) {
                listOfRecipes.add(recipeToDTO(reicpe));
            }
        }

        return listOfRecipes;
    }

}
