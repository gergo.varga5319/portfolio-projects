package com.recipeBook.controller;


import com.recipeBook.domain.RecipeDTO;
import com.recipeBook.service.RecipeBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;


import java.util.List;
import java.util.Optional;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.HttpStatus.OK;

@SpringBootApplication
@RestController
@RequestMapping(value = "/api/matritel/recipebook/recipes")
public class RecipeBookController {


    @Autowired
    private RecipeBookService recipeBookService;




    @PostMapping
    @ResponseStatus(CREATED)
    public RecipeDTO postTodo(@RequestBody RecipeDTO recipeDTOInput){
        return recipeBookService.create(recipeDTOInput);
    }




    @GetMapping
    @ResponseStatus(OK)
    @ResponseBody
    public List<RecipeDTO> getAllRecipes(){
        return recipeBookService.findAll();
    }



    @GetMapping(value = "/{id}")
    @ResponseStatus(OK)
    @ResponseBody
    public Optional<RecipeDTO> getById(@PathVariable String id) {
        if(recipeBookService.findByID(id).isPresent()){
            return  recipeBookService.findByID(id);
        }else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST ,"THIS IS A VERY BAD REQUEST");
        }

    }


    @DeleteMapping(value = "/{id}")
    @ResponseStatus(NO_CONTENT)
    @ResponseBody
    public void deleteByID(@PathVariable String id){
        if (recipeBookService.findByID(id).isPresent()){
            recipeBookService.deleteById(id);
        }
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,"The recipe with this id cannot be found!");
    }


    @GetMapping(value = "/findByCreator/{creator}")
    @ResponseStatus(OK)
    @ResponseBody
    public List<RecipeDTO> findByCreator(@PathVariable String creator){
        if (!recipeBookService.findByCreator(creator).isEmpty()){
            return  recipeBookService.findByCreator(creator);
        }else
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"Cannot find recipe by the given creator");

    }

}
