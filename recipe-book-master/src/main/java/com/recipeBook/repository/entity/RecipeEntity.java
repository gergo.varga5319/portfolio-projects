package com.recipeBook.repository.entity;

import com.recipeBook.domain.IngredientDTO;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity(name = "recipes")
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class RecipeEntity {

    @Id
    @GeneratedValue(generator = "uuid2") // this is a specific id generator in Hibernate
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    private String id;


    @Column(name = "title", columnDefinition = "VARCHAR(255)")
    private String title;

    @Column(name = "instruction", columnDefinition = "VARCHAR(255)")
    private String instructions;


    @ElementCollection
    @Embedded
    @Column(name = "recipeIngredients",columnDefinition = "VARCHAR(255)")
    private List<IngredientDTO> ingredients;


    @Column(name = "creatorName",columnDefinition = "VARCHAR(255)")
    private String creatorName;

    @Column(name = "date")
    private LocalDate localDate;
}
