package com.recipeBook.repository;

import com.recipeBook.repository.entity.RecipeEntity;
import org.springframework.data.repository.CrudRepository;

public interface RecipeBookRepository extends CrudRepository<RecipeEntity,String> {
}
