package com.recipeBook.domain;


import lombok.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Builder
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor


public class RecipeDTO {



    private String id;


    @Size(max = 255)
    @NotNull
    private String title;

    @Size(max = 255)
    @NotNull
    private String instructions;

    @Size(max = 255)
    @NotNull
    private List<IngredientDTO> ingredients;


    @Size(max = 20)
    @NotNull
    private String creatorName;

    private LocalDate localDate;


}
