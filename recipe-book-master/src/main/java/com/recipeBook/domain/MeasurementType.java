package com.recipeBook.domain;

public enum MeasurementType {
    GRAMS,MILLILITER
}
