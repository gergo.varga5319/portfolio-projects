package com.recipeBook.domain;


import lombok.*;

import javax.persistence.Embedded;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class IngredientDTO {

//    @Size(max = 20)
//    @NotNull
//    private String ingredients;

    @Size(max = 20)
    private String name;

    @Embedded
    @NotNull
    private MeasurementDTO measurement;

}
