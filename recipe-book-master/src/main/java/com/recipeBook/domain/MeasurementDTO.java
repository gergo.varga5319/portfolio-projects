package com.recipeBook.domain;


import lombok.*;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

public class MeasurementDTO {

    @Size(max = 10)
    @NotNull
    private int amount;

    @Size(max = 20)
    @NotNull
    @Enumerated(EnumType.STRING)
    private MeasurementType type;
}
