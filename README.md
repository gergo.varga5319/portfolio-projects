# Portfolio Projects

## Welcome to my personal porjects repository.


#### Summary of the repository:
    * Cd Tray:
        * A simple OOP exercise to get the hang of it.
    * Hibernate:
        * A simple Hibernate exercise where you can create different types of Persons and save their data in the database.
    * JDBC:
        * A simple JDBC exercise the same as the hibernate one but here I used JDBC instead.
    * Recipe book:
        * A basic spring exercise to get the hang of the Spring Framework.
    * Todo app:
        * A simple Spring Boot app where you can create reminder for chores that you have to do.
        * It is using hibernate to connect with the database.
        * Thymeleaf for backend(coming soon)
    * Android app:
        * A simple android application which interracts with the blockchain called ZBS network;
        * Using the connection with the network it can authenticate the crendentials for those who would like to log in.


If your have any questions please feel free contanct me on "gergo.varga5319@gmail.com";