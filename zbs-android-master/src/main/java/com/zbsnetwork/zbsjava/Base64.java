package com.zbsnetwork.zbsjava;

public class Base64 {
    public static String encode(byte[] input) {
        return "base64:" + new String(java.util.Base64.getEncoder().encode(input));
    }

    public static byte[] decode(String input) {
        if (input.startsWith("base64:")) input = input.substring(7);
        return java.util.Base64.getDecoder().decode(input);
    }
}
