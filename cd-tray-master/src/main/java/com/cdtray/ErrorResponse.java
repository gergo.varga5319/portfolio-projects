package com.cdtray;

public class ErrorResponse extends Exception{
    public ErrorResponse(String message) {
        super(message);
    }
}
